/**
 * The modal
 * @requires ./App.vue
 * @component
 * @property {Array} dataExperimented - Represents the data of the experienced technologies
 * @property {Array} dataFamiliar - Represents the data of the familiar technologies
 * @property {Function} close - Allows you to close the modal
 * <Modal
    :dataExperimented="this.dataExperimented"
    :dataFamiliar="this.dataFamiliar"
    v-show="isModalVisible"
    @close="closeModal"
  ></Modal>
 */
export default {
  name: "Modale",
  props: {
    /**
     * Data parameter of experienced technologies
     * @model
     * @values array of objects
     */
    dataExperimented: {
      type: Array,
      require: true,
    },
    /**
     * Data parameter of familiar technologies
     * @values array of objects
     */
    dataFamiliar: {
      type: Array,
      require: true,
    },
    /**
     * Data parameter of details of each experiment after clicking on the information circle
     * @values Object or undefined
     */
    modalDisplayExperience: {
      type: Object || undefined,
      require: true,
    },
  },
  /**
   * Init of modal data
   * @property {String} modaleTitle - Title in modal header
   * @property {Boolean} active class css modal-size for max size 968px
   * @returns {Object}
   */
  data() {
    return {
      modaleTitle: "",
      isActive: false,
    };
  },
  // At a change of data we update the modal experience, the title the modal, and its max size 968px
  updated() {
    this.setModalTitle();
    this.isActive = this.modalDisplayExperience !== undefined ? true : false;
  },
  methods: {
    /**
     * @function
     * @name close Call closeModal fonction in parent App.vue script (data bidings)
     * @type {void}
     * @public
     */
    close() {
      // Close modal event.
      this.$emit("close");
    },
    /**
     * @function
     * @name setModalTitle update modal title after clicking on the information circle experience
     * @type {void}
     * @public
     */
    setModalTitle() {
      this.modaleTitle =
        this.modalDisplayExperience == undefined
          ? "COMPÉTENCES"
          : this.modalDisplayExperience.title +
            " " +
            "-" +
            " " +
            this.modalDisplayExperience.company;
    },
  },
};

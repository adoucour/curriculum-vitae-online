import { createApp } from "vue";
import App from "./App.vue";

createApp(App).mount("#app");

/**
 * Listen a click on toogle for show menu
 * @constant @type {Function}
 * @param {String} toggleId id toggle show menu in html element
 * @param {String} navId id nav menu in html element
 */
const showMenu = (toggleId, navId) => {
  const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId);

  if (toggle && nav) {
    toggle.addEventListener("click", () => {
      nav.classList.toggle("show-menu");
    });
  }
};
showMenu("nav-toggle", "nav-menu");

/**
 * Remove menu mobile
 * Content All nav link in HTMLElement
 * @constant @type {HTMLElement}
 */
const navLink = document.querySelectorAll(".nav__link");

/**
 * @function
 * @name linkAction - This function refers to the section of the item selected in nav menu
 * @type {void}
 *
 */
function linkAction() {
  const navMenu = document.getElementById("nav-menu");
  // When we click on each nav__link, we remove the show-menu class
  navMenu.classList.remove("show-menu");
}
navLink.forEach((n) => n.addEventListener("click", linkAction));

/**
 * Scroll sections active link
 * @constant @type {HTMLElement}
 */
const sections = document.querySelectorAll("section[id]");

/**
 * @function
 * @name scrollActive
 * @type {void}
 */
function scrollActive() {
  const scrollY = window.pageYOffset;

  sections.forEach((current) => {
    const sectionHeight = current.offsetHeight;
    const sectionTop = current.offsetTop - 50;
    const sectionId = current.getAttribute("id");

    if (scrollY > sectionTop && scrollY <= sectionTop + sectionHeight) {
      document
        .querySelector(".nav__menu a[href*=" + sectionId + "]")
        .classList.add("active-link");
    } else {
      document
        .querySelector(".nav__menu a[href*=" + sectionId + "]")
        .classList.remove("active-link");
    }
  });
}
window.addEventListener("scroll", scrollActive);

/**
 * @function
 * @name scrollTop - Show scroll top
 * @type {void}
 */
function scrollTop() {
  const scrollTop = document.getElementById("scroll-top");
  // When the scroll is higher than 560 viewport height, add the show-scroll class to the a tag with the scroll-top class
  if (this.scrollY >= 200) scrollTop.classList.add("show-scroll");
  else scrollTop.classList.remove("show-scroll");
}
window.addEventListener("scroll", scrollTop);

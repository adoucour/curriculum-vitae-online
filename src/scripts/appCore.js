import html2pdf from "html2pdf.js";
import UserData from "../assets/json/param.json";
import Modal from "../components/Modal.vue";

/**
 * Header, Body
 */
const app = {
  name: "App",
  components: { Modal },

  /**
   * @function
   * @property {HTMLElement} areaCV - Main html content
   * @property {Object} opt - Param for generate PDF
   * @property {HTMLElement} themeButton - Logo switch theme html content
   * @property {String} darkTheme - Dark theme string
   * @property {String} iconTheme - Icon theme string
   * @property {String} selectedTheme - User currently select theme
   * @property {String} selectedIcon - Icon displayed on the screen according to the chosen theme
   * @property {Object} params - Param data file * @file - "/assets/json/param.json"
   * @property {Boolean} isModalVisible - Boolean for display or not the modal
   * @property {Array} dataExperimented - Array of objects that represent the experienced skills
   * @property {Array} dataFamiliar - Array of objects that represent the familiar skills
   * @property {String} availabilityDate - Availability date
   * @property {Object} modalDisplayExperience - detailed data of experiments
   * @property {String} numberPhoneFr - French phone number with space
   * @returns {Object}
   */
  data() {
    return {
      areaCV: undefined,
      opt: undefined,
      themeButton: undefined,
      darkTheme: "dark-theme",
      iconTheme: "bx-sun",
      selectedTheme: "",
      selectedIcon: "",
      params: UserData,
      isModalVisible: false,
      dataExperimented: undefined,
      dataFamiliar: undefined,
      availabilityDate: "",
      modalDisplayExperience: undefined,
      numberPhoneFr: "",
    };
  },

  computed: {
    /**
     * @function
     * @name lastEducationLine Get last element in array of object for not to display last education__line in the education section
     * @type {Number}
     * @public
     * @returns The last lenght of the object education data
     */
    lastEducationLine() {
      return Object.keys(this.params.data.education).length - 1;
    },

    /**
     * @function
     * @name last Get last element in array of object for not to display last experiences in the education section
     * @type {Number}
     * @public
     * @returns The last lenght of the object experiences data
     */
    lastExperiencesLine() {
      return Object.keys(this.params.data.experiences).length - 1;
    },
  },

  methods: {
    /**
     * @function
     * @name scaleCv - Reduce the size and print on an A4 sheet
     * @type {void}
     * @public
     */
    scaleCv() {
      document.body.classList.add("scale-cv");
    },
    /**
     * @function
     * @name generateResume - Call Generate pdf function in html2pdf.js
     * @type {void}
     * @public
     */
    generateResume() {
      /**
       * Check out {@link https://ekoopmans.github.io/html2pdf.js/}
       * @function
       * @name html2pdf - Generate pdf file
       * @param {HTMLElement} this.areaCV - Body content html which will be generated in pdf
       * @param {Object} this.opt - Param
       */
      html2pdf(this.areaCV, this.opt);
    },
    /**
     * @function
     * @name removeScale - Reset the size
     * @type {void}
     * @public
     */
    removeScale() {
      document.body.classList.remove("scale-cv");
    },
    /**
     * @function
     * @name downloadCv - Download cv on his device
     * @type {void}
     * @public
     */
    downloadCv() {
      // not print pdf with a dark theme if this is the default theme
      let switchThemeTemp = localStorage.getItem("selected-theme") === "dark";
      if (switchThemeTemp) {
        this.darkModeSwitch();
      }

      this.scaleCv();
      this.generateResume();
      setTimeout(() => {
        this.removeScale();
        // restore dark theme if this is the default theme
        if (switchThemeTemp) {
          this.darkModeSwitch();
        }
      }, 2000);
    },
    /**
     * @function
     * @name darkModeSwitch - Call function dark/light theme switch
     * @type {void}
     * @public
     */
    darkModeSwitch() {
      themeChange(
        this.$data.themeButton,
        this.$data.darkTheme,
        this.$data.iconTheme
      );
    },
    /**
     * @function
     * @name redirectLinkSocial - At the click, redirects to a link in a new tab
     * @param {String} type condition for redirect to the right link
     * @type {void}
     * @public
     */
    redirectLinkSocial(type) {
      switch (type) {
        case "linkedin":
          window.open(this.params.data.linkedInLink, "_blank");
          break;
        case "github":
          window.open(this.params.data.gitHubLink, "_blank");
          break;
        case "gitlab":
          window.open(this.params.data.gitLabLink, "_blank");
          break;
        default:
          break;
      }
    },
    /**
     * @function
     * @name mailTo - On click, redirects to the email application
     * @type {void}
     * @public
     */
    mailTo() {
      let mySubject = "Votre profil";
      document.location =
        "mailto:" + this.params.data.mail + "?subject=" + mySubject;
    },
    /**
     * @function
     * @name redirectPhone - On click, redirects to the calling application with the phone number
     * @type {void}
     * @public
     */
    redirectPhone() {
      window.open("tel:" + this.params.data.phoneNumber);
    },
    /**
     * @function
     * @name showModal - Set the flag isModalVisible to close the modal
     * @param {Object} content - detailed data of experiments clicked
     * @type {void}
     * @public
     */
    showModal(content) {
      this.isModalVisible = true;
      this.modalDisplayExperience = content;
    },
    /**
     * @function
     * @name closeModal - Set the flag isModalVisible to display the modal
     * @type {void}
     * @public
     */
    closeModal() {
      this.isModalVisible = false;
    },
    /**
     * @function
     * @name setDataForModal - Set variables to the bindings for modal
     * @binding {array} this.dataExperimented
     * @binding {array} this.dataFamiliar
     * @type {void}
     * @public
     */
    setDataForModal() {
      this.dataExperimented = this.params.data.modalDataExperimented;
      this.dataFamiliar = this.params.data.modalDataFamiliar;
    },
    /**
     * Check out {@link https://ekoopmans.github.io/html2pdf.js/}
     * @function
     * @name initParamsPdf - Init object param for generate PDF (second param in function html2pdf)
     * @type {void}
     * @public
     */
    initParamsPdf() {
      let body = document.body;
      let html = document.documentElement;
      let height = Math.max(
        body.scrollHeight,
        body.offsetHeight,
        html.clientHeight,
        html.scrollHeight,
        html.offsetHeight
      );

      let heightCM = height / 35.35;

      // param for generate pdf
      this.opt = {
        margin: 0,
        filename: this.params.data.filename,
        image: { type: "jpeg", quality: 0.98 },
        html2canvas: {
          scale: 2,
          dpi: 192,
          letterRendering: true,
        },
        jsPDF: {
          unit: "cm",
          format: [heightCM, 30],
          orientation: "portrait",
        },
      };
    },
  },
  created() {
    this.initParamsPdf();
    this.setDataForModal();
    this.availabilityDate = getDateAvailaible();
    this.numberPhoneFr = spaceNumberPhone(this.params.data.phoneNumber);
  },
  mounted() {
    this.areaCV = document.getElementById("area-cv");
    this.themeButton = document.getElementById("theme-button");
    this.selectedTheme = localStorage.getItem("selected-theme");
    this.selectedIcon = localStorage.getItem("selected-icon");
    restoreSelectedTheme(
      this.selectedTheme,
      this.selectedIcon,
      this.themeButton
    );
  },
};

export default app;

/**
 * @constant @type {Function}
 * @param {String} darkTheme - Dark theme string
 * @private
 * @returns A string to designate whether one is in dark theme or light theme
 */
const getCurrentTheme = (darkTheme) =>
  document.body.classList.contains(darkTheme) ? "dark" : "light";
/**
 * @constant @type {Function}
 * @param {String} iconTheme - Icon theme string
 * @param {HTMLElement} themeButton - Logo switch theme html content
 * @private
 * @returns A string to designate whether the current theme icon
 */
const getCurrentIcon = (iconTheme, themeButton) =>
  themeButton.classList.contains(iconTheme) ? "bx-moon" : "bx-sun";

/**
 * @function
 * @name themeChange - Activate / deactivate the theme manually with the button
 * @param {HTMLElement} themeButton - Logo switch theme html content
 * @param {String} darkTheme - Dark theme string
 * @param {String} iconTheme - Icon theme string
 * @type {void}
 * @private
 */
function themeChange(themeButton, darkTheme, iconTheme) {
  // Add or remove the dark / icon theme
  document.body.classList.toggle(darkTheme);
  themeButton.classList.toggle(iconTheme);
  // We save the theme and the current icon that the user chose
  localStorage.setItem("selected-theme", getCurrentTheme(darkTheme));
  localStorage.setItem("selected-icon", getCurrentIcon(iconTheme, themeButton));
}

/**
 * @function
 * @name restoreSelectedTheme - Restores the selected theme at page launch
 * @param {String} selectedTheme - User currently select theme
 * @param {String} selectedIcon - Icon displayed on the screen according to the chosen theme
 * @param {HTMLElement} themeButton - Logo switch theme html content
 * @type {void}
 * @private
 */
function restoreSelectedTheme(selectedTheme, selectedIcon, themeButton) {
  let darkTheme = app.data().darkTheme;
  let iconTheme = app.data().iconTheme;
  // We validate if the user previously chose a topic
  if (selectedTheme) {
    // If the validation is fulfilled, we ask what the issue was to know if we activated or deactivated the dark
    document.body.classList[selectedTheme === "dark" ? "add" : "remove"](
      darkTheme
    );
    themeButton.classList[selectedIcon === "bx-moon" ? "add" : "remove"](
      iconTheme
    );
  }
}

/**
 * @function
 * @name getDateAvailaible - Allows you to obtain the availability date (4 months after today’s date)
 * @type {String}
 * private
 * @returns Availability date in string
 */
function getDateAvailaible() {
  let result;

  let dateAvailaible = new Date();
  let dateAvailaibleTimestampConvert;
  let dateAvailaibleFinal;

  dateAvailaible = dateAvailaible.setMonth(dateAvailaible.getMonth() + 3);
  dateAvailaibleTimestampConvert = new Date(dateAvailaible);
  dateAvailaibleFinal =
    dateAvailaibleTimestampConvert.getMonth() +
    1 +
    "/" +
    dateAvailaibleTimestampConvert.getDate() +
    "/" +
    dateAvailaibleTimestampConvert.getFullYear();

  const options = { year: "numeric", month: "long", day: "numeric" };
  result = new Date(dateAvailaibleFinal).toLocaleDateString("fr-FR", options);

  return result;
}

/**
 * @function
 * @param {*} num - Puts the number in the format fr with a space every two digits
 * @type {String}
 * @private
 * @returns Number in the format fr (example : 06 01 02 03 04) to string
 */
function spaceNumberPhone(num) {
  let FrTel = (tel, sep) => tel.replace(/(\d\d(?!$))/g, "$1" + sep);

  return FrTel(num, " ");
}
